/* Copyright (c) 2023 Vinícius dos Santos Oliveira

   Distributed under the Boost Software License, Version 1.0. (See accompanying
   file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt) */

#include <emilua/async_base.hpp>
#include <emilua/native_module.hpp>

#include <td/telegram/td_json_client.h>
#include <trial/circular/array.hpp>

#include <syslog.h>

#include <condition_variable>
#include <thread>
#include <mutex>
#include <cmath>

namespace emilua_tdlib {

namespace asio = boost::asio;
namespace hana = boost::hana;

using emilua::vm_context;
using emilua::get_vm_context;
using emilua::rawgetp;
using emilua::setmetatable;
using emilua::finalizer;
using emilua::push;
using emilua::set_interrupter;

static char client_mt_key;

struct control_block_t
{
    static constexpr std::size_t QUEUE_CAPACITY = 16;

    std::mutex mutex;
    trial::circular::array<std::string, QUEUE_CAPACITY> messages;
    std::condition_variable messages_is_not_full_cond;
    std::shared_ptr<vm_context> vm_ctx;
    lua_State* waiting_fiber = nullptr;
};

struct handle_type
{
    handle_type(vm_context& vm_ctx)
        : control_block{std::make_shared<control_block_t>()}
    {
        client = td_json_client_create();

        thread = std::jthread{
            [
                client=this->client,control_block=this->control_block,
                work_guard=vm_ctx.work_guard()
            ](std::stop_token stoken) {
                while (!stoken.stop_requested()) {
                    const char* reply = td_json_client_receive(
                        client, /*timeout_in_seconds=*/HUGE_VAL);
                    if (!reply)
                        continue;

                    std::unique_lock lk{control_block->mutex};
                    while (control_block->messages.full()) {
                        control_block->messages_is_not_full_cond.wait(lk);
                        if (stoken.stop_requested())
                            return;
                    }

                    control_block->messages.push_back(reply);

                    if (control_block->waiting_fiber != nullptr) {
                        auto vm_ctx = control_block->vm_ctx;
                        assert(vm_ctx);
                        vm_ctx->strand().post([vm_ctx,control_block]() {
                            lua_State* fiber = nullptr;
                            std::string msg;

                            {
                                std::lock_guard lk{control_block->mutex};

                                fiber = control_block->waiting_fiber;
                                if (!fiber || control_block->messages.empty())
                                    return;

                                control_block->vm_ctx.reset();
                                control_block->waiting_fiber = nullptr;

                                if (control_block->messages.full()) {
                                    control_block->messages_is_not_full_cond
                                        .notify_one();
                                }

                                msg = control_block->messages.pop_front();
                            }

                            vm_ctx->fiber_resume(
                                fiber,
                                hana::make_set(
                                    hana::make_pair(
                                        vm_context::options::arguments,
                                        hana::make_tuple(std::nullopt, msg))));
                        }, std::allocator<void>{});
                    }
                }
            }
        };
    }

    ~handle_type()
    {
        {
            std::lock_guard lk{control_block->mutex};
            control_block->vm_ctx.reset();
            control_block->waiting_fiber = nullptr;
        }

        thread.request_stop();
        td_json_client_send(client, R"({"@type":"testUseUpdate"})");
        control_block->messages_is_not_full_cond.notify_all();
        thread.join();
        td_json_client_destroy(client);
    }

    void* client;
    std::shared_ptr<control_block_t> control_block;
    std::jthread thread;
};

class plugin : emilua::native_module
{
public:
    void init_appctx(
        const std::unique_lock<std::shared_mutex>&,
        emilua::app_context& appctx) noexcept override;
    std::error_code init_lua_module(
        std::shared_lock<std::shared_mutex>&, emilua::vm_context& vm_ctx,
        lua_State* L) override;

    static emilua::app_context* appctx;
};

emilua::app_context* plugin::appctx;

struct log_domain;

} // namespace emilua_tdlib
template<>
struct emilua::log_domain<emilua_tdlib::log_domain>
{
    static constexpr std::string_view name = "tdlib";
    static int log_level;
};
int emilua::log_domain<emilua_tdlib::log_domain>::log_level = LOG_CRIT;
namespace emilua_tdlib {

void plugin::init_appctx(
    const std::unique_lock<std::shared_mutex>&,
    emilua::app_context& appctx) noexcept
{
    plugin::appctx = &appctx;
    appctx.init_log_domain<log_domain>();
    std::string message = fmt::format(
        R"({{"@type":"setLogVerbosityLevel","new_verbosity_level":{}}})",
        ::emilua::log_domain<log_domain>::log_level);
    td_execute(message.c_str());
    td_set_log_message_callback(
        ::emilua::log_domain<log_domain>::log_level,
        [](int verbosity_level, const char *message) {
            plugin::appctx->log<log_domain>(verbosity_level, "{}", message);
        });
}

static int client_new(lua_State* L)
{
    auto& vm_ctx = get_vm_context(L);

    auto h = static_cast<handle_type*>(lua_newuserdata(L, sizeof(handle_type)));
    rawgetp(L, LUA_REGISTRYINDEX, &client_mt_key);
    setmetatable(L, -2);
    new (h) handle_type{vm_ctx};

    return 1;
}

static int client_send(lua_State* L)
{
    lua_settop(L, 2);

    auto handle = static_cast<handle_type*>(lua_touserdata(L, 1));
    if (!handle || !lua_getmetatable(L, 1)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }
    rawgetp(L, LUA_REGISTRYINDEX, &client_mt_key);
    if (!lua_rawequal(L, -1, -2)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }

    if (lua_type(L, 2) != LUA_TSTRING) {
        push(L, std::errc::invalid_argument, "arg", 2);
        return lua_error(L);
    }

    td_json_client_send(handle->client, lua_tostring(L, 2));
    return 0;
}

static int client_receive(lua_State* L)
{
    auto handle = static_cast<handle_type*>(lua_touserdata(L, 1));
    if (!handle || !lua_getmetatable(L, 1)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }
    rawgetp(L, LUA_REGISTRYINDEX, &client_mt_key);
    if (!lua_rawequal(L, -1, -2)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }

    auto& vm_ctx = get_vm_context(L);
    auto current_fiber = vm_ctx.current_fiber();
    EMILUA_CHECK_SUSPEND_ALLOWED(vm_ctx, L);

    std::lock_guard lk{handle->control_block->mutex};

    if (handle->control_block->waiting_fiber) {
        push(L, std::errc::device_or_resource_busy, "arg", 1);
        return lua_error(L);
    }

    if (handle->control_block->messages.empty()) {
        lua_pushvalue(L, 1);
        lua_pushlightuserdata(L, vm_ctx.current_fiber());
        lua_pushcclosure(L, [](lua_State* L) -> int {
            auto handle = static_cast<handle_type*>(
                lua_touserdata(L, lua_upvalueindex(1)));
            auto current_fiber = static_cast<lua_State*>(
                lua_touserdata(L, lua_upvalueindex(2)));

            std::lock_guard lk{handle->control_block->mutex};

            if (current_fiber != handle->control_block->waiting_fiber)
                return 0;

            auto vm_ctx = handle->control_block->vm_ctx;
            handle->control_block->vm_ctx.reset();
            handle->control_block->waiting_fiber = nullptr;

            vm_ctx->strand().post([vm_ctx,current_fiber]() {
                vm_ctx->fiber_resume(
                    current_fiber,
                    hana::make_set(
                        hana::make_pair(
                            vm_context::options::arguments,
                            hana::make_tuple(emilua::errc::fiber_canceled))));
            }, std::allocator<void>{});
            return 0;
        }, 2);
        set_interrupter(L, vm_ctx);

        handle->control_block->vm_ctx = vm_ctx.shared_from_this();
        handle->control_block->waiting_fiber = current_fiber;
        return lua_yield(L, 0);
    }

    if (handle->control_block->messages.full()) {
        handle->control_block->messages_is_not_full_cond.notify_one();
    }

    auto message = handle->control_block->messages.pop_front();
    lua_pushnil(L);
    push(L, message);
    return 2;
}

std::error_code plugin::init_lua_module(
    std::shared_lock<std::shared_mutex>&, emilua::vm_context& /*vm_ctx*/,
    lua_State* L)
{
    lua_createtable(L, /*narr=*/0, /*nrec=*/1);
    {
        lua_pushliteral(L, "new");
        lua_pushcfunction(L, client_new);
        lua_rawset(L, -3);
    }

    lua_pushlightuserdata(L, &client_mt_key);
    lua_createtable(L, /*narr=*/0, /*nrec=*/3);
    {
        lua_pushliteral(L, "__metatable");
        lua_pushliteral(L, "tdlib.client");
        lua_rawset(L, -3);

        lua_pushliteral(L, "__index");
        {
            lua_createtable(L, /*narr=*/0, /*nrec=*/2);

            lua_pushliteral(L, "send");
            lua_pushcfunction(L, client_send);
            lua_rawset(L, -3);

            lua_pushliteral(L, "receive");
            {
                emilua::rawgetp(
                    L, LUA_REGISTRYINDEX,
                    &emilua::var_args__retval1_to_error__fwd_retval2__key);
                rawgetp(L, LUA_REGISTRYINDEX, &emilua::raw_error_key);
                lua_pushcfunction(L, client_receive);
                lua_call(L, 2, 1);
            }
            lua_rawset(L, -3);
        }
        lua_rawset(L, -3);

        lua_pushliteral(L, "__gc");
        lua_pushcfunction(L, finalizer<handle_type>);
        lua_rawset(L, -3);
    }
    lua_rawset(L, LUA_REGISTRYINDEX);

    return {};
}

} // namespace emilua_tdlib

extern "C" BOOST_SYMBOL_EXPORT emilua_tdlib::plugin EMILUA_PLUGIN_SYMBOL;
emilua_tdlib::plugin EMILUA_PLUGIN_SYMBOL;
